 
topleft="1"
topcenter = "2"
topright = "3"
centerleft = "4"
center = "5"
centeright = "6" 
bottomleft = "7"
bottomcenter = "8"
bottomright = "9"
print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
print("                                   |                                         |")                               
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |") 
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("___________________________________|_________________________________________|________________________________________")
print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("___________________________________|_________________________________________|________________________________________")
print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
print("                                   |                                         |")
choice = input("Where would you like to make your move?")

if(choice == "square 1"):
    print("")
    print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
    print("                                   |                                         |")                               
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("               X                   |                                         |")
    print("                                   |                                         |") 
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")

if(choice == "square 2"):
    print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
    print("                                   |                                         |")                               
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                     X                   |")
    print("                                   |                                         |") 
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    
if(choice == "square 3"):
    print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
    print("                                   |                                         |")                               
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |                        X")                     
    print("                                   |                                         |") 
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")

if(choice == "square 4"):
    print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
    print("                                   |                                         |")                               
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |") 
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                   X               |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")

if(choice == "square 5"):
    print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
    print("                                   |                                         |")                               
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |") 
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                      X                  |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("___________________________________|_________________________________________|________________________________________")
    print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    print("                                   |                                         |")
    
    if(choice == "square 6"):
        print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
        print("                                   |                                         |")                               
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |") 
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |                             X")                     
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
    
    if(choice == "square 7"):
        print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
        print("                                   |                                         |")                               
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |") 
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("               X                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        
    if(choice == "square 8"):
        print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
        print("                                   |                                         |")                               
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |") 
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                       X                 |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        
    if(choice == "square 9"):
        print(topleft+"                                  | "+topcenter+"                                       |   "+topright)  
        print("                                   |                                         |")                               
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |") 
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(centerleft+"                                  |    "+center+"                                    |    "+centeright)     
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("___________________________________|_________________________________________|________________________________________")
        print(bottomleft+"                                  |   "+bottomcenter+"                                     |   "+bottomright)
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |                        X")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")
        print("                                   |                                         |")